<?php

namespace App\Services;
interface DataProviderInterface
{
    public function readFiles();
    public function filterByStatus($status,$dataProvider);
    public function filterByCurrency($currency,$data);
    public function filterByRange($min,$max,$data);
    public function filterByAll($params);
}
