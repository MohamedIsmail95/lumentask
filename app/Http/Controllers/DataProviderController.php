<?php

namespace App\Http\Controllers;

use App\Classes\Response;
use App\Classes\DataHandler;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Illuminate\Http\Response as ResponseCode;

class DataProviderController extends Controller
{
    use ApiResponser;
    public function index()
    {
        $data = DataHandler::checkData();
        $response = new Response(['data'=>$data],ResponseCode::HTTP_OK);
        return $this->validResponse($response);
    }
    public function filters(Request $request)
    {
        DataHandler::validation($request);
        $filterData = DataHandler::filterAll($request->all());
        $response = new Response(['data'=>$filterData],ResponseCode::HTTP_OK);
        return $this->validResponse($response);
    }

}
