<?php
namespace App\Classes;

use App\Services\DataProviderInterface;

class DataProviderHandler implements DataProviderInterface
{
    public function readFiles($provider=Null)
    {
        if(isset($provider)) {
            $files[] = $provider.'.json';
        }
        else{
            $files = ['DataProviderW.json','DataProviderX.json','DataProviderY.json'];
        }

        foreach($files as $file)
        {
            $url = storage_path($file);
            $readJSONFile = file_get_contents($url);
            $foo = utf8_encode($readJSONFile);
            $data[] = json_decode($foo, true);
        }
        return $data;
    }
    public function filterByAll($params=[])
    {
        $min=NULL; $max=NULL;
        $filteredData = [];
        foreach ($params as $key=>$value) {
            if ($key == "provider") {
                $filteredData = $this->readFiles($value);
            }
        }
        if(empty($filteredData))
        {
            $filteredData = $this->readFiles();
        }
        foreach ($params as $key=>$value) {
            if ($key == "currency") {
                $filteredData = $this->filterByCurrency($value,$filteredData);
            }
            if ($key == "statusCode") {
                $filteredData = $this->filterByStatus($value,$filteredData);
            }
            if ($key == "amountMin") {
                $min = $value;
            }
            if($key=="amountMax"){
                $max = $value;
            }
        }
        if(isset($min) && isset($max))
        {
            $filteredData = $this->filterByRange($min,$max,$filteredData);
        }
        return $filteredData;
    }
    public function filterByStatus($status,$dataProvider)
    {
        $filteredData = [];
        foreach ($dataProvider as $datas)
        {
            foreach ($datas as $data)
            {
                if (isset($data['status'])&&(($data['status'] == 'done' && $status == 'paid') ||
                    ($data['status'] == 'wait' && $status == 'pending') ||
                    ($data['status'] == 'nope' && $status == 'reject')))
                {
                    $filteredData[] = $data;
                }

                elseif (isset($data['transactionStatus']) && (($data['transactionStatus'] == 1 && $status == 'paid') ||
                    ($data['transactionStatus'] == 2 && $status == 'pending') ||
                    ($data['transactionStatus'] == 3 && $status == 'reject')))
                {
                    $filteredData[] = $data;
                }
                elseif (isset($data['status'])&&(($data['status'] == 100 && $status == 'paid') ||
                    ($data['status'] == 200 && $status == 'pending') ||
                    ($data['status'] == 300 && $status == 'reject')))
                {
                    $filteredData[] = $data;
                }
            }
        }
        return [$filteredData];
    }

    public function filterByCurrency($currency, $dataProvider)
    {
        $filteredData = [];
        foreach ($dataProvider as $datas)
        {
            foreach ($datas as $data)
            {
                if($data['currency'] == $currency)
                {
                    $filteredData[] = $data;
                }
            }
        }
        return [$filteredData];
    }
    public function filterByRange($min, $max, $dataProvider)
    {
        $filteredData = [];
        foreach ($dataProvider as $datas)
        {
            foreach ($datas as $data)
            {
                if((isset($data['amount'])&& ($data['amount'] >= $min&& $data['amount']<=$max)) ||
                    (isset($data['transactionAmount'])&&($data['transactionAmount'] >= $min&& $data['transactionAmount']<=$max)))
                {
                    $filteredData[] = $data;
                }
            }
        }
        return [$filteredData];
    }

}
