<?php
namespace App\Classes;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class DataHandler extends DataProviderHandler
{
    public static function checkData()
    {
        $data = DataHandler::getData();
        return $data;
    }
    public static function getData($provider=NULL)
    {
        $handler = new self();
        $data = $handler->readFiles($provider);
        return $data;
    }
    public static function filterStatus($status,$data)
    {
        $handler = new self();
        $statusData = $handler->filterByStatus($status,$data);
        return $statusData;
    }
    public static function filterCurrency($currency,$data)
    {
        $handler = new self();
        $currency = $handler->filterByCurrency($currency,$data);
        return $currency;
    }
    public static function filterAmountRange($min,$max,$data)
    {
        $handler = new self();
        $range = $handler->filterByRange($min,$max,$data);
        return $range;
    }
    public static function filterAll($params)
    {
        $handler = new self();
        $filters = $handler->filterByAll($params);
        return $filters;
    }
    public static function validation(Request $request)
    {
        $validatedData = Validator::make($request->all(),[
            'provider' => 'sometimes|in:DataProviderW,DataProviderX,DataProviderY',
            'statusCode' => 'sometimes|in:paid,pending,reject',
            'currency'=>'sometimes|min:3|max:3',
            'amountMin'=>'sometimes',
            'amountMax'=>'sometimes'
        ]);
        if($validatedData->fails())
        {
            throw ValidationException::withMessages($validatedData->errors()->toArray());
        }
    }

}
